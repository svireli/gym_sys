<?php

use Base\Plan as BasePlan;

/**
 * Skeleton subclass for representing a row from the 'Plans' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Plan extends BasePlan
{

}
