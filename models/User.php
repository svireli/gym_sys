<?php

use Base\User as BaseUser;

/**
 * Skeleton subclass for representing a row from the 'Users' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class User extends BaseUser
{
    private static $currentUser;
    
    public static function createWithNameAndPassword($name, $password, $needsHash = true) {
       $user = new \User();
       $user->setName($name);
       $user->setPassword($needsHash ? self::generateHash($password) : $password);
       return $user;
    }
    
    public function exists() {
        $users = \UserQuery::create()
        ->filterByName($this->getName())
        ->filterByPassword($this->getPassword())
        ->find();
        if($users->isEmpty()) {
            return null;
        } else {
            return $users->getFirst();
        }
    }
    
    public static function setCurrentUser($user) {
        self::$currentUser = $user;
    }
    
    public static function getCurrentUser() {
        if(self::$currentUser)
            return self::$currentUser;
        else {
            $user = self::createWithNameAndPassword(self::getUserNameFromCookie(), self::getUserPasswordFromCookie(), false);
            self::$currentUser = $user->exists();
            return self::$currentUser;
        }
    }
    
    public function saveUserNameInCookie() {
        setcookie("name", $this->getName(), $this->generateCookieTime(1));
    }
    
    public static function getUserNameFromCookie() {
        return $_COOKIE['name'] ?? $_COOKIE['name'] ?? null;
    }
    
    public static function getUserPasswordFromCookie() {
        return $_COOKIE['password'] ?? $_COOKIE['password'] ?? null;
    }
    
    public function saveUserInCookie($remember = false) {
        $this->saveUserNameInCookie();
        if($remember)
            setcookie("password", $this->getPassword(), $this->generateCookieTime(1));
        else
            setcookie("password", $this->getPassword(), $this->generateCookieTime(0, 1));
    }
    
    public function logout() {
        unset($_COOKIE['password']);
        setcookie('password', null, -1, '/');
    }
    
    public function setRawPassword($password) {
        $this->setPassword(self::generateHash($password));
    }
    
    public static function generateHash($input) {
        return hash('sha256', $input);
    }
    
    public function generateCookieTime($year = 0, $days = 0, $hours = 0, $minutes = 0, $seconds = 0) {
        return time() + ($year * 365 * 24 * 60 * 60) + ($days * 24 * 60 * 60) + ($hours * 60 * 60) + ($minutes * 60) + $seconds;
    }
}
