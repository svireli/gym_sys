<?php

use Base\PlanQuery as BasePlanQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'Plans' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class PlanQuery extends BasePlanQuery
{

}
