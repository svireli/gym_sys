<?php

use Base\Lift as BaseLift;

/**
 * Skeleton subclass for representing a row from the 'Lifts' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Lift extends BaseLift
{

}
