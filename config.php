<?php

/*
Server: sql11.freemysqlhosting.net
Name: sql11178505
Username: sql11178505
Password: h7zvZLbVdy
Port number: 3306
*/

$config = [
    'settings' => [
        'logger' => [
            'name' => 'st_app',
            'file' => __DIR__ . '/logs/app.log',
            'level' => \Monolog\Logger::DEBUG
        ],
        'view' => [
            'path' => __DIR__ . '/views',
        ],
        'displayErrorDetails' => true
    ]
];
