<?php
namespace Middlewares;
class Json
{
    public function __invoke($request, $response, $next)
    {
        return $next(
            $request,
            $response->withHeader('Content-type', 'application/json')
        );
    }
}
