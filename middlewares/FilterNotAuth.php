<?php
namespace Middlewares;

class FilterNotAuth {
    private $container;

    function  __construct($container) {
        $this->container = $container;
    }

    public function __invoke($request, $response, $next) {
        if(\User::getCurrentUser())
            return $next($request, $response);
        return $response->withStatus(302)->withHeader('Location', '/login');
    }
}
