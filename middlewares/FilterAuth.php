<?php
namespace Middlewares;

class FilterAuth {
    private $container;

    function  __construct($container) {
        $this->container = $container;
    }

    public function __invoke($request, $response, $next) {
        if (\User::getCurrentUser())
            return $response->withStatus(302)->withHeader('Location', '/customers');
        return $next($request, $response);
    }
}
