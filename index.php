<?php
session_start();

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require './vendor/autoload.php';
require_once 'config.php';
require_once './propel-conf/config.php';
$container = new \Slim\Container($config);

$container['view'] = new League\Plates\Engine($container['settings']['view']['path']);
$container['logger'] = new \Monolog\Logger($container['settings']['logger']['name']);
$container['flash'] = function () {
    return new \Slim\Flash\Messages();
};

$container->get('logger')->pushHandler(
    new \Monolog\Handler\StreamHandler(
        $container['settings']['logger']['file'],
        $container['settings']['logger']['level']
    )
);

$serviceContainer->setLogger('defaultLogger', $container->get('logger'));
$app = new \Slim\App($container);

$app->get('/', \Controllers\User::class.':login')->add(new \Middlewares\FilterAuth($container));
$app->get('/login', \Controllers\User::class.':login')->add(new \Middlewares\FilterAuth($container));
$app->post('/login', \Controllers\User::class.':postLogin')->add(new \Middlewares\FilterAuth($container));
$app->get('/logout', \Controllers\User::class.':logout')->add(new \Middlewares\FilterNotAuth($container));

$app->get('/customers', \Controllers\Customer::class.':listAll')->add(new \Middlewares\FilterNotAuth($container));
$app->get('/customer/new', \Controllers\Customer::class.':create')->add(new \Middlewares\FilterNotAuth($container));
$app->post('/customer/save', \Controllers\Customer::class.':save')->add(new \Middlewares\FilterNotAuth($container));
$app->get('/customer/{id}/image', \Controllers\Customer::class.':image')->add(new \Middlewares\FilterNotAuth($container));
$app->get('/customer/{id}', \Controllers\Customer::class.':individualPage')->add(new \Middlewares\FilterNotAuth($container));
$app->get('/customer/{id}/delete', \Controllers\Customer::class.':delete')->add(new \Middlewares\FilterNotAuth($container));


$app->get('/payment/{id}/delete', \Controllers\Plan::class.':delete')->add(new \Middlewares\FilterNotAuth($container));
$app->post('/payment/save', \Controllers\Plan::class.':save')->add(new \Middlewares\FilterNotAuth($container));



/*$app->post('/customer/toggle_status', \Controllers\Customer::class.':postToggleIsActive')
    ->add(new \Middlewares\FilterNotAuth($container))
    ->add(new \Middlewares\Json());*/

$app->get('/plans', \Controllers\Plan::class.':listAll')->add(new \Middlewares\FilterNotAuth($container));

$app->run();
