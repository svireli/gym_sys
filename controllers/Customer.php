<?php

namespace Controllers;

use Propel\Runtime\ActiveQuery\Criteria;

class Customer{
    protected $container;

    public function __construct($container) {
        $this->container = $container;
    }
    
    public function delete($request, $response, $args) {
        $customer = \CustomerQuery::create()->findPK($args['id']);
        if($customer){
            \PaymentQuery::create()->filterByCustomerId($customerId)->delete();
            \LiftQuery::create()->filterByCustomerId($customerId)->delete();
            \CustomerdayQuery::create()->filterByCustomerId($customerId)->delete();
            $customer->delete();
        }
        return $response->withStatus(302)->withHeader('Location', '/customers');
    }
    
    public function individualPage($request, $response, $args) {
        $customer = \CustomerQuery::create()->findPK($args['id']);
        if($customer){
            $plans = \PlanQuery::create()->find();
            $paymentsQuery = \PaymentQuery::create()->filterByCustomerId($customer->getId());
            return $this->container->get('view')->render('customers/individual', [
                    'customer' => $customer,
                    'plans' => $plans,
                    'payments' => $paymentsQuery->find(),
                    'paymentCount' => $paymentsQuery->count()
                ]);
        }
        return $response->withStatus(302)->withHeader('Location', '/customers');
    }
    
    public function image($request, $response, $args) {
        $content = "";
        $imageType = "image/jpg";
        if(isset($args['id'])){
            $user = \CustomerQuery::create()->findPK($args['id']);
            if($user) {
                $content = stream_get_contents($user->getImage());
                $imageType = $user->getImageType();
            }
        }
        $response->write($content);
        return $response->withHeader('Expires', '0')
                    ->withHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0')
                    ->withHeader('Pragma', 'public')
                    ->withHeader('Content-Type', $imageType)
                    ->withHeader('Content-Length', strlen($content));
    }
    
    public function save($request, $response, $args) {
        $customer = new \Customer();
        $customerId = $request->getParam('customerId');
        if($customerId) {
            $customer = \CustomerQuery::create()->findPK($customerId);
            if(!$customer) {
                $customer = new \Customer();
            }
        }
        $customer->setFirstName($request->getParam('firstName'));
        $customer->setLastName($request->getParam('lastName'));
        $customer->setPhone($request->getParam('phone'));
        $customer->setRegistrationDate($request->getParam('registrationDate'));
        $customer->setGender($request->getParam('gender'));
        $customer->setIsActive($request->getParam('isActive'));
        $customer->setPlanId($request->getParam('planId'));
        $files = $request->getUploadedFiles();
        
        if(isset($files['image']) && $files['image']->getSize()){
            $content = fread(fopen($files['image']->file, 'r'), filesize($files['image']->file));
            $customer->setImage($content);
            $customer->setImageType($files['image']->getClientMediaType());
        }
        $customer->save();
        if($request->getParam('createFirstPayment')) {
            $payment = new \Payment();
            $payment->setPlanId($customer->getPlanId());
            $payment->setCustomerId($customer->getId());
            $payment->setDate(date('Y-m-d'));
            $payment->setAmount($customer->getPlan()->getPrice());
            $payment->save();
        }
        return $response->withStatus(302)->withHeader('Location', '/customer/'.($customer->getId()));
    }
    
    public function create($request, $response, $args) {
        $plans = \PlanQuery::create()->find();
        return $this->container->get('view')->render('customers/new', [
            'plans' => $plans,
            'registrationDate' => date("Y-m-d")
            ]);
    }
    
    public function postToggleIsActive($request, $response, $args) {
        $customerId = $request->getParam('customerId');
        $customer = \CustomerQuery::create()->findPK($customerId);
        $success = false;
        if($customer) {
            $customer->toggleIsActive();
            $customer->save();
            $success = true;
        }
        return json_encode(['success' => $success]);
    }
    
    public function listAll($request, $response, $args) {
        $customersQuery = \CustomerQuery::create();
        $plans = \PlanQuery::create()->find();
        $fullName = $request->getParam('fullName');
        $customersQuery->filterByFirstname('%'.$request->getParam('firstName').'%', Criteria::LIKE);
        $customersQuery->filterByLastname('%'.$request->getParam('lastName').'%', Criteria::LIKE);
        $customersQuery->filterByPhone('%'.$request->getParam('phone').'%', Criteria::LIKE);
        $gender = $request->getParam('gender');
        if($gender == '0' || $gender == '1')
            $customersQuery->filterByGender($gender);
        $isActive = $request->getParam('isActive');
        if($isActive == '0' || $isActive == '1')
            $customersQuery->filterByIsActive($isActive);
        $planId = $request->getParam('planId');
        if(!is_null($planId))
            $customersQuery->filterByPlanId($planId);
        $registrationDateFrom = $request->getParam('registrationDateFrom');
        $registrationDateTo = $request->getParam('registrationDateTo');
        $customersQuery->filterByRegistrationDate([
                'min' => $registrationDateFrom ? $registrationDateFrom : "01-01-1121",
                'max' => $registrationDateTo ? $registrationDateTo : "01-01-3121"
            ]);
    
        $customers = $customersQuery->find();
        return $this->container->get('view')->render('customers/list', [
            'customers' => $customers,
            'plans' => $plans,
            'firstName' => $firstName,
            'lastName' => $lastName,
            'phone' => $phone,
            'registrationDateFrom' => $registrationDateFrom,
            'registrationDateTo' => $registrationDateTo,
            'gender' => $gender,
            'phone' => $phone,
            'planId' => $planId,
            'isActive' => $isActive,
            'customerCount' => $customersQuery->count()
            ]);
    }
    
}
