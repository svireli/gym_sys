<?php
namespace Controllers;
class User{
    protected $container;

    public function __construct($container) {
        $this->container = $container;
    }

    public function postLogin($request, $response, $args) {
        $name = $request->getParam('name');
        $password = $request->getParam('password');
        $remember = $request->getParam('remember');
        
        $user = \User::createWithNameAndPassword($name, $password);
        $dbUser = $user->exists();
        
        if ($dbUser){
            $user->saveUserInCookie($remember);
            return $response->withStatus(302)->withHeader('Location', '/customers');
        } else {
            $user->saveUserNameInCookie();
            $this->container->get('logger')->info('Wrong Username or Password: '.$name.' '.$password);
            $this->container->get('flash')->addMessage('error', true);
            return $response->withStatus(302)->withHeader('Location', '/login');
        }
    }
    
    public function login($request, $response, $args) {
        $messages = $this->container->get('flash')->getMessages();
        return $this->container->get('view')->render('users/login', [
            'error' => $messages['error'] ?? $messages['error'] ?? false,
            'name' => \User::getUserNameFromCookie()
            ]);
    }
    
    public function logout($request, $response, $args) {
        \User::getCurrentUser()->logout();
        return $response->withStatus(302)->withHeader('Location', '/login');
    }

}
