<?php
namespace Controllers;
class Plan{
    protected $container;

    public function __construct($container) {
        $this->container = $container;
    }

    public function listAll($request, $response, $args) {
        $plans = \PlanQuery::create()->find();
        return $this->container->get('view')->render('plans/list', ['plans' => $plans]);
    }
    
    
    public function delete($request, $response, $args) {
        $payment = \PaymentQuery::create()->findPK($args['id']);
        if($payment){
            $payment->delete();
            return $response->withStatus(302)->withHeader('Location', '/customer/'.$payment->getCustomer()->getId());
        }
        return $response->withStatus(302)->withHeader('Location', '/customers');
    }
    
    public function save($request, $response, $args) {
        $customer = \CustomerQuery::create()->findPK($request->getParam('customerId'));
        $plan = \PlanQuery::create()->findPK($request->getParam('planId'));
        if($customer && $plan){
            $payment = new \Payment();
            $payment->setCustomerId($customer->getId());
            $payment->setPlanId($plan->getId());
            $payment->setAmount($request->getParam('amount'));
            $payment->setDate($request->getParam('date'));
            $payment->save();
            return $response->withStatus(302)->withHeader('Location', '/customer/'.$customer->getId());
        }
        return $response->withStatus(302)->withHeader('Location', '/customers');
    }
    
}
