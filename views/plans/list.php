<?php $this->layout('admin', ['title' => 'პაკეტები']) ?>

<?php $this->start('main_content') ?>
    <style type="text/css">
        @media only screen and (max-width : 768px) {
            #plans-container {
                overflow-x: scroll;
            }
        }
    </style>
    <div id="plans-container" class="container-fluid">
        <div clas="row">
            <div class="col-md-12">
                <table class="table">
                  <caption>მოძიებული პაკეტები</caption>
                  <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th class="text-center">სახელი</th>
                        <th class="text-center">ფასი(თვეში)</th> 
                        <th class="text-center">დღეები(თვეში)</th>
                        <th class="text-center">სტატუსი</th>
                        <th class="text-center">ხელსაწყოები</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($plans as $plan): ?>
                        <tr class="<?=$this->e($plan->getIsActive() ? '' : 'danger')?>">
                            <td class="text-center"><?=$this->e($plan->getId())?></td>
                            <td class="text-center"><?=$this->e($plan->getTitle())?></td>
                            <td class="text-center"><?=$this->e($plan->getPrice())?></td>
                            <td class="text-center"><?=$this->e($plan->getDays())?></td>
                            <td class="text-center"><?=$this->e($plan->getIsActive() ? 'აქტიური' : 'პასიური')?></td>
                            <td class="text-center">
                                <a class="btn btn-primary btn-sm"
                                    style="margin:0 auto;"
                                    href="/plans/<?=$this->e($plan->getId())?>" 
                                    role="button" 
                                    data-toggle="tooltip" 
                                    title="დაწვრილებით">
                                    <i class="fa fa-address-card-o"></i>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach ?>
                  </tbody>
                </table>
            </div>
        </div>
    </div>
<?php $this->stop('main_content') ?>