<table class="table">
    <caption>მოძიებული გადახდები (<?=$this->e($paymentCount)?>)</caption>
    <thead>
        <tr>
            <th class="text-center">#</th>
            <th class="text-center">სახელი</th>
            <th class="text-center">გვარი</th>
            <th class="text-center">პაკეტი</th>
            <th class="text-center">გადახდის თარიღი</th>
            <th class="text-center">თანხა</th>
            <th class="text-center">ხელსაწყოები</th>
        </tr>
    </thead>
    <tbody>
        <?php if($payments): ?>
            <?php foreach($payments as $payment): ?>
                <tr customer-id="<?=$this->e($payment->getId())?>">
                    <td class="text-center"><?=$this->e($payment->getId())?></td>
                    <td class="text-center"><?=$this->e($payment->getCustomer()->getFirstName())?></td>
                    <td class="text-center"><?=$this->e($payment->getCustomer()->getLastName())?></td>
                    <td class="text-center"><?=$this->e($payment->getPlan()->getTitle())?></td>
                    <td class="text-center"><?=$this->e($payment->getDate()->format('Y-m-d'))?></td>
                    <td class="text-center"><?=$this->e($payment->getAmount())?></td>
                    <td class="text-center">
                        <a class="btn btn-danger btn-sm"
                            href="/payment/<?=$this->e($payment->getId())?>/delete" 
                            role="button" 
                            data-toggle="tooltip" 
                            title="გადახდის წაშლა">
                            <i class="fa fa-trash-o"></i>
                        </a>
                    </td>
                </tr>
            <?php endforeach ?>
        <?php endif ?>
    </tbody>
</table>