<form method="POST" action="/payment/save">
    <div class="row">
        <div class="col-md-3">
            <input type="hidden" name="customerId" value="<?=(isset($customerId) ? $customerId : '')?>">
            <?=$this->insert('shared/plans_dropdown', [ 'planId' => $planId, 'plans' => $plans ])?>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="date">გადახდის თარიღი</label>
                <input type="date" class="form-control" id="date" name="date" 
                    value="<?=$date?>">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="amount">თანხა</label>
                <input type="number" min="0" class="form-control" id="amount" name="amount" 
                    value="<?=$amount?>">
            </div>
        </div>
        <div class="col-md-3">
            <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> <span class=""> დამატება</span></button>
            <button type="reset" class="btn btn-info"><i class="fa fa-trash-o"></i> <span class=""> გასუფთავება</span></button>
        </div>
    </div>
</form>