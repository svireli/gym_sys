<form method="GET">
    <div class="row">
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-6">
                    <?=$this->insert('shared/firstName_input', [ 'firstName' => $firstName ])?>
                </div>
                <div class="col-md-6">
                    <?=$this->insert('shared/lastName_input', [ 'lastName' => $lastName ])?>  
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?=$this->insert('shared/phone_input', [ 'phone' => $phone ])?>
                </div>
                <div class="col-md-6">
                    <?=$this->insert('shared/plans_dropdown', [ 'planId' => $planId, 'plans' => $plans ])?>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-6">
                    <?=$this->insert('shared/gender_dropdown', [ 'gender' => $gender ])?>
                </div>
                <div class="col-md-6">
                    <?=$this->insert('shared/statuses_dropdown', [ 'isActive' => $isActive ])?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?=$this->insert('shared/dateRange_input', [ 'label' => "რეგისტრაციის თარიღის შუალედი", 
                    'dateFrom' => $registrationDateFrom, 'dateTo' => $registrationDateTo,
                    'idFrom' => 'registrationDateFrom', 'idTo' => 'registrationDateTo', 'nameFrom' => 'registrationDateFrom', 'nameTo' => 'registrationDateTo'])?>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> <span class="hidden-xs hidden-md"> ძიება</span></button>
            <a href="/customers" class="btn btn-info"><i class="fa fa-trash-o"></i> <span class="hidden-xs hidden-md"> გასუფთავება</span></a>
            <a href="/customer/new" class="btn btn-success pull-right"><i class="fa fa-plus"></i> <span class="hidden-xs hidden-md"> ახლის დამატება</span></a>
        </div>
    </div>
</form>
