<?php $this->layout('admin', ['title' => 'მომხმარებლები']) ?>

<?php $this->start('main_content') ?>
    <style type="text/css">
        @media only screen and (max-width : 768px) {
            .payments-grid {
                overflow-x: scroll;
            }
        }
    </style>
    <div class="container-fluid" style="margin: 1% 0 1% 0;">
        <div class="row">
            <div class="col-md-12">
                <h4>მომხმარებლის ინფორმაცია</h4>
            </div>
            <?php if($plans && $customer): ?>
                <div class="col-md-1">
                    <a  href="/customer/<?=$this->e($customer->getId())?>/image" 
                        data-lightbox="customer-image-<?=$this->e($customer->getId())?>" 
                        data-title="<?=$this->e($customer->getFirstName())?>">
                        <img class="img-thumbnail" src="/customer/<?=$customer->getId()?>/image">
                    </a>
                </div>
                <div class="col-md-11" style="margin: 1% 0 1% 0;">
                        <?=$this->insert('customers/form', [
                        'plans' => $plans,
                        'firstName' => $customer->getFirstName(),
                        'lastName' => $customer->getLastName(),
                        'phone' => $customer->getPhone(),
                        'gender' => $customer->getGender(),
                        'isActive' => $customer->getIsActive(),
                        'planId' => $customer->getPlanId,
                        'registrationDate' => $customer->getRegistrationDate()->format('Y-m-d'),
                        'customerId' => $customer->getId(),
                        'hideFirstPaymentRegistration' => true,
                        'showDeleteButton' => true
                        ])?>
                </div>
            <?php endif ?>
            <?php if($payments): ?>
                <div class="col-md-12">
                    <h5>მომხმარებლის გადახდები</h5>
                    <?=$this->insert('payments/form', [
                        'customerId' => $customer->getId(),
                        'plans' => $plans,
                        'planId' => $customer->getPlanId(),
                        'date' => date('Y-m-d'),
                        'amount' => $customer->getPlan()->getPrice()
                    ])?>
                </div>
                <div class="col-md-12 payments-grid">
                    <?=$this->insert('payments/grid', [
                        'payments' => $payments,
                        'paymentCount' => $paymentCount
                    ])?>
                </div>
            <?php endif ?>
        </div>
    </div>
<?php $this->stop('main_content') ?>