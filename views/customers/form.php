<form enctype="multipart/form-data" method="POST" action="/customer/save">
    <div class="modal fade" id="deleteConfirmationModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="დახურვა"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="modalLabel">შეტყობინება</h4>
          </div>
          <div class="modal-body">
              <p>მომხმარებლის წაშლის შემთხვევაში წაიშლება ყველა ჩანაწერი რომელშიც მომხმარებელი მონაწილეობს (გადახდები, ვარჯიშები, დღეები)</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">დახურვა</button>
            <a href="/customer/<?=(isset($customerId) ? $customerId : '')?>/delete" class="btn btn-danger"><i class="fa fa-trash"></i> <span class="hidden-xs hidden-md"> მომხმარებლის წაშლა</span></a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-6">
                    <input type="hidden" name="customerId" value="<?=(isset($customerId) ? $customerId : '')?>">
                    <?=$this->insert('shared/firstName_input', [ 'firstName' => $firstName ])?>
                </div>
                <div class="col-md-6">
                    <?=$this->insert('shared/lastName_input', [ 'lastName' => $lastName ])?>  
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?=$this->insert('shared/phone_input', [ 'phone' => $phone ])?>
                </div>
                <div class="col-md-6">
                    <?=$this->insert('shared/plans_dropdown', [ 'planId' => $planId, 'plans' => $plans ])?>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-6">
                    <?=$this->insert('shared/gender_dropdown', [ 'gender' => $gender, 'hideBothOption' => true ])?>
                </div>
                <div class="col-md-6">
                    <?=$this->insert('shared/statuses_dropdown', [ 'isActive' => $isActive, 'hideBothOption' => true ])?>  
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?=$this->insert('shared/date_input', [ 'label' => 'რეგისტრაციის თარიღი', 'date' => $registrationDate, 'id' => 'registrationDate', 'name' => 'registrationDate' ])?>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="image">სურათი</label>
                        <input type="file" name="image" id="image" accept=".png,.jpg">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <?php if(isset($hideFirstPaymentRegistration) && $hideFirstPaymentRegistration): ?>
            <?php else: ?>
                <div class="checkbox">
                    <label>
                        <input  name="createFirstPayment" type="checkbox"> პირველი გადახდის ავტომატურად შექმნა
                    </label>
                </div>
            <?php endif ?>
            <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> <span class=""> შენახვა</span></button>
            <button type="reset" class="btn btn-info"><i class="fa fa-trash-o"></i> <span class=""> გასუფთავება</span></button>
            <?php if(isset($showDeleteButton) && $showDeleteButton): ?>
                <a data-toggle="modal" data-target="#deleteConfirmationModal" class="btn btn-danger">
                    <i class="fa fa-trash"></i> <span class="hidden-xs hidden-md">წაშლა</span>
                </a>
            <?php endif ?>
        </div>
    </div>
</form>