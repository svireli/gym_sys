<?php $this->layout('admin', ['title' => 'მომხმარებლის შექმნა']) ?>

<?php $this->start('main_content') ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h4>ახალი მომხმარებლის შექმნა</h4>
            </div>
            <div class="col-md-12" style="margin: 1% 0 1% 0;">
                <?=$this->insert('customers/form', ['plans' => $plans, 'registrationDate' => $registrationDate])?>
            </div>
        </div>
    </div>
<?php $this->stop('main_content') ?>