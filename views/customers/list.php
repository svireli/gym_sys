<?php $this->layout('admin', ['title' => 'მომხმარებლები']) ?>

<?php $this->start('main_content') ?>
    <style type="text/css">
        @media only screen and (max-width : 768px) {
            .customers-grid {
                overflow-x: scroll;
            }
        }
    </style>
    <div class="container-fluid">
        <div clas="row">
            <div class="col-md-12">
                <?php $this->insert('customers/search_form', [
                    'firstName' => $firstName,
                    'lastName' => $lastName,
                    'phone' => $phone,
                    'registrationDateFrom' => $registrationDateFrom,
                    'registrationDateTo' => $registrationDateTo,
                    'gender' => $gender,
                    'phone' => $phone,
                    'planId' => $planId,
                    'isActive' => $isActive,
                    'plans' => $plans
                ]) ?>
            </div>
            <?php if($customers->isEmpty()): ?>
                <div class="col-md-4 col-md-offset-4 text-center">
                    <h4>მომხმარებლები არ მოიძებნა</h4>
                </div>
            <?php else: ?>
                <div class="col-md-12 customers-grid">
                    <?=$this->insert('customers/grid', [ 'customers' => $customers, 'customerCount' => $customerCount ])?>
                </div>
            <?php endif ?>
        </div>
    </div>
<?php $this->stop('main_content') ?>