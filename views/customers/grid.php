<table class="table">
    <caption>მოძიებული მომხმარებლები (<?=$this->e($customerCount)?>)</caption>
    <thead>
        <tr>
            <th class="text-center">#</th>
            <th class="text-center">სახელი</th>
            <th class="text-center">გვარი</th> 
            <th class="text-center">ტელეფონი</th>
            <th class="text-center">სქესი</th>
            <th class="text-center">სურათი</th>
            <th class="text-center">პაკეტი</th>
            <th class="text-center">რეგისტრაციის თარიღი</th>
            <th class="text-center">სტატუსი</th>
            <th class="text-center">ხელსაწყოები</th>
        </tr>
    </thead>
    <tbody>
        <?php if($customers): ?>
            <?php foreach($customers as $customer): ?>
                <tr customer-id="<?=$this->e($customer->getId())?>" class="<?=$this->e($customer->getIsActive() ? '' : 'danger')?>">
                    <td class="text-center"><?=$this->e($customer->getId())?></td>
                    <td class="text-center"><?=$this->e($customer->getFirstName())?></td>
                    <td class="text-center"><?=$this->e($customer->getLastName())?></td>
                    <td class="text-center"><?=$this->e($customer->getPhone())?></td>
                    <td class="text-center"><?=$this->e($customer->getGender() ? 'მამრობითი' : 'მდედრობითი')?></td>
                    <td class="text-center">
                        
                    </td>
                    <td class="text-center"><?=$this->e($customer->getPlan()->getTitle())?>(<?=$this->e($customer->getPlan()->getPrice())?>)</td>
                    <td class="text-center"><?=$this->e($customer->getRegistrationDate()->format('Y-m-d'))?></td>
                    <td class="text-center"><?=$this->e($customer->getIsActive() ? 'აქტიური' : 'პასიური')?></td>
                    <td name="tools" class="text-center">
                        <a class="btn btn-primary btn-sm"
                            href="/customer/<?=$this->e($customer->getId())?>" 
                            role="button" 
                            data-toggle="tooltip" 
                            title="დაწვრილებით">
                            <i class="fa fa-address-card-o"></i>
                        </a>
                        <a class="btn btn-success btn-sm"
                            data-toggle="tooltip"
                            title="მომხმარებლის სურათი"
                            href="/customer/<?=$this->e($customer->getId())?>/image" 
                            data-lightbox="customer-image-<?=$this->e($customer->getId())?>" 
                            data-title="<?=$this->e($customer->getFirstName())?>"><i class="fa fa-image"></i></a>
                    </td>
                </tr>
            <?php endforeach ?>
        <?php endif ?>
    </tbody>
</table>