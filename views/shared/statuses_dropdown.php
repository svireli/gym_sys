<div class="form-group">
    <label for="isActive">სტატუსი</label>
    <select class="form-control" id="isActive" name="isActive">
      <?php if(isset($hideBothOption) && $hideBothOption): ?>
      <?php else: ?>
      <?php if(!isset($isActive)) $isActive = ''; ?>
      <option value="" <?=($isActive == ''? 'selected':'')?>>ორივე</option>
      <?php endif ?>
      <option value="1" <?=($isActive == '1'? 'selected':'')?>>აქტიური</option>
      <option value="0" <?=($isActive == '0'? 'selected':'')?>>პასიური</option>
    </select>
</div>