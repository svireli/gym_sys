<div class="form-group">
    <label for="plan">პაკეტი</label>
    <select class="form-control" id="plan" name="planId">
        <?php if($showEmptryOption): ?>
            <option value="">
                ყველა
            </option>
        <?php endif ?>
        <?php foreach($plans as $plan): ?>
            <option value="<?=$this->e($plan->getId())?>" <?=($plan->getId() == $planId? 'selected':'')?>>
                <?=$this->e($plan->getTitle())?>(
                <?=$plan->getPrice()?>,
                <?=($plan->getIsActive() ? 'აქტ.' : 'პას.')?> )
            </option>
        <?php endforeach ?>
    </select>
</div>