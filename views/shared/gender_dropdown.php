<div class="form-group">
    <label for="gender">სქესი</label>
    <select class="form-control" id="gender" name="gender">
      <?php if(isset($hideBothOption) && $hideBothOption): ?>
      <?php else: ?>
      <?php if(!isset($isActive)) $isActive = ''; ?>
      <option value="" <?=($isActive == ''? 'selected':'')?>>ორივე</option>
      <?php endif ?>
      <option vlue="1" <?=($isActive == '1'? 'selected':'')?>>მამრობითი</option>
      <option value="0" <?=($isActive == '0'? 'selected':'')?>>მდედრობითი</option>
    </select>
</div>