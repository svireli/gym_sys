<?php $this->layout('template', ['title' => 'სისტემაში შესვლა']) ?>

<?php $this->start('header') ?>
<style>
    body {
        /*background: url('/static/images/mcgrath.png') no-repeat center center fixed;*/
        background-color: #3273dc;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
        overflow: hidden;
    }
    #login-container {
        margin-top: 15%;
    }
</style>
<?php $this->stop('header') ?>

<?php $this->start('body') ?>
  <div id="login-container" class="container">
    <div class="row">
      <div class="col-md-4 col-md-offset-4">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">მართვის სისტემა</h3>
            </div>
            <div class="panel-body">
              <?php if ($error): ?>
                  <div class="alert alert-danger" role="alert"><strong>შეცდომა!</strong> მომხმარებლის სახელი ან პაროლი არასწორია.</div>
              <?php endif ?>
              <form method="POST" action="/login">
                <div class="form-group">
                  <label for="name_input">სახელი</label>
                  <input type="text" class="form-control" name="name" value="<?=$this->e($name)?>" id="name_input" placeholder="მომხმარებლის სახელი">
                </div>
                <div class="form-group">
                  <label for="password_input">პაროლი</label>
                  <input type="password" class="form-control" name="password" id="password_input" placeholder="მომხმარებლის პაროლი">
                </div>
                <div class="checkbox">
                  <label>
                    <input name="remember" type="checkbox"> დამახსოვრება
                  </label>
                </div>
                <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-user"></i> შესვლა</button>
              </form>
            </div>
          </div>
      </div>
    </div>
  </div>
<?php $this->stop('body') ?>
