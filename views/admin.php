<?php $this->layout('template', ['title' => $title]) ?>

<?php $this->start('body') ?>
    
    <nav class="navbar navbar-inverse">
      <div class="container-fluid">
        
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navigation-menu" aria-expanded="false">
            <span class="sr-only">მენიუს ჩამოშლა</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/customers">GMS</a>
        </div>
    
        <div class="collapse navbar-collapse" id="main-navigation-menu">
          <ul class="nav navbar-nav">
            <li><a href="/customers"><i class="fa fa-users"></i> მომხმარებლები</a></li>
            <li><a href="/payments"><i class="fa fa-btc"></i> გადახდები</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">დამატებით <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="/plans"><i class="fa fa-users"></i> პაკეტები</a></li>
                <li><a href="/statistics"><i class="fa fa-bar-chart"></i> სტატისტიკა</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="/lifts"><i class="fa fa-list-alt"></i> შედეგების დაფა</a></li>
                <li><a href="/workouts"><i class="fa fa-heartbeat"></i> ვარჯიშები</a></li>
                <li><a href="/weekdays"><i class="fa fa-calendar-o"></i> კვირის დღეები</a></li>
              </ul>
            </li>
          </ul>
          <form class="navbar-form navbar-left hidden-sm hidden-md" method="GET" action="/customers">
            <div class="input-group">
              <input name="firstName" size="25" type="text" class="form-control" placeholder="მომხმარებლის სწრაფი ძიება">
              <span class="input-group-btn">
                <button type="submit" class="btn btn-default pull-right"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="/user"><i class="fa fa-user-circle-o"></i> პროფილი</a></li>
            <li><a href="/logout"><i class="fa fa-power-off"></i> სისტემიდან გასვლა</a></li>
          </ul>
        </div>
      </div>
    </nav>
    
    <div id="main_content">
        <?=$this->section('main_content')?>        
    </div>
    
    <div style="background-color: #3273dc; height: 10px" class="container-fluid">
    </div>
    <script type="text/javascript">
      $("[data-toggle='tooltip'").tooltip();
      $(document).ready(function() {
          var url = $(location).attr('href');
          $("#main-navigation-menu li a").each(function(){
            var me = $(this);
            var parent = me.closest('li');
            parent.removeClass('active');
            if(url.indexOf(me.attr('href')) != -1) {
              parent.addClass('active');
            }
          });
      });
    </script>
<?php $this->stop('body') ?>